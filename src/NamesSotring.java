public class NamesSotring {
    public static void main(String[] args) {
        String[] names = new String[]{"Taras", "Timur", "Taras", "Artur", "Ilya", "Dmitry", "Kanstantsin", "Mikita"};
        String[] surnames = new String[]{"Rybin", "Verkhavodka", "Arishanovich", "Podpalov", "Ivanov", "Pytaylo", "Kernazhytski", "Hashkin"};
        BubbleSort(names, surnames);
        for (int i = 0; i < names.length; i++)
            System.out.println(names[i] + " " + surnames[i]);
    }

    public static void BubbleSort(String[] names, String[] surnames) {
        boolean isSorted = false;
        String buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = names.length - 1; i > 0; i--) {
                if (names[i].equals(names[i - 1]) && surnames[i].compareTo(surnames[i - 1]) < 0) {
                    isSorted = false;
                    buf = names[i - 1];
                    names[i - 1] = names[i];
                    names[i] = buf;
                    buf = surnames[i - 1];
                    surnames[i - 1] = surnames[i];
                    surnames[i] = buf;
                }
                if (names[i].compareTo(names[i - 1]) < 0) {
                    isSorted = false;
                    buf = names[i - 1];
                    names[i - 1] = names[i];
                    names[i] = buf;
                    buf = surnames[i - 1];
                    surnames[i - 1] = surnames[i];
                    surnames[i] = buf;

                }
            }
        }
    }
}

